<?php

namespace Drupal\webform_group_extended\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\webform\WebformInterface;
use Symfony\Component\HttpFoundation\RedirectResponse as HttpFoundationRedirectResponse;

/**
 * Restrict form to access only for groups the user is a member of.
 */
class WebformSelectAccessGroupForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_group_extended_webform_select_access_group';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL) {
    $form_state->set('webform', $webform);
    $element = $webform->getElementDecoded('webform_access_group');
    $targetTypes = isset($element['#selection_settings']['target_bundles']) ? array_keys($element['#selection_settings']['target_bundles']) : [];

    $groupMembershipRequired = !isset($element['#require_group_membership']) || $element['#require_group_membership'] == TRUE;
    $rolesRequired = $element['#require_group_roles'] ?? NULL;

    $options = [];
    if ($groupMembershipRequired) {
      // Get user's groups:
      $grp_membership_service = Drupal::service('group.membership_loader');
      $memberships = $grp_membership_service->loadByUser(User::load(Drupal::currentUser()
        ->id()));
      foreach ($memberships as $membership) {
        $group = $membership->getGroup();
        if (isset($targetTypes) && count($targetTypes) > 0) {
          if (in_array($group->bundle(), $targetTypes)) {
            if (!$rolesRequired || array_intersect(array_keys($membership->getRoles()), $rolesRequired)) {
              $options[$group->id()] = $group->label();
            }
          }
        }
        else {
          $options[$group->id()] = $group->label();
        }
      }

      if (count($options) == 0) {
        $form['markup'] = [
          '#markup' => $this->t('To complete this form, you need to be a member of a relevant group on this site.'),
        ];
        unset($form['actions']['#submit']);
        return $form;
      }
    }

    $form['markup'] = [
      '#markup' => $this->t('To complete this form, you need to select a group that the form will apply to.'),
    ];

    if (count($options) == 0) {
      $form['webform_access_group'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'group',
        '#required' => TRUE,
        '#title' => $this->t('Search and select group'),
        '#description' => $this->t('Start typing and select group from list.'),
      ];
      if (count($targetTypes) > 0) {
        $form['webform_access_group']['#selection_settings'] = [
          'target_bundles' => $targetTypes,
        ];
      }
    }
    else {
      $form['webform_access_group'] = [
        '#type' => 'select',
        '#title' => $this->t('Select group'),
        '#required' => TRUE,
        '#options' => $options,
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue to form'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $url = NULL;
    if ($destination = Drupal::request()->query->get('destination')) {
      $url = Url::fromUri('internal:' . $destination);
    }

    if (!$url) {
      $webform = $form_state->get('webform');
      $url = $webform->toUrl();
    }

    // setRedirectUrl doesn't keep query string for some reason...
    $url->setOption('query', ['webform_access_group' => $form_state->getValue('webform_access_group')]);
    $response = new HttpFoundationRedirectResponse($url->toString());
    $response->send();
  }

}
